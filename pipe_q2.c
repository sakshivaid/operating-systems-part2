/* Pipe question 2 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define NO_SIZE 1
main() {
	int p[2], j, m;
	char *n;
	pid_t pid;

	printf("Enter a number\n");
	scanf("%s", n);

	/* open pipe */

	if(pipe(p) == -1) {

		perror("pipe call");
		exit(1);
	}	
	
	pid = fork();

	if( pid == -1 ) {
		perror("Fork Failure");
		exit(2);
	}

	else if(pid > 0) {
		
		close(p[1]);
		write(p[0], n,NO_SIZE);
		close(p[1]);
	}
	
	else {

		close(p[0]);

		
		m = atoi(read(p[1],n,NO_SIZE));

		for(j = 1; j <= m; j+=2)
			printf("%d\n", j); 

	}

	exit(0);


}
